const cover = document.getElementById('cover')
const start = document.getElementById('start')
const main = document.getElementById('main')
const myPlane = document.getElementById('myPlane')
const score = document.querySelector('.score')
const stopGame = document.querySelector('.stopGame')
const last = document.querySelector('.last')
const pphh = document.querySelector('.pphh progress')
let timer = null
myPlane.setAttribute('myph', 20)
//游戏结束
function gameover() {
	const enemys = main.getElementsByClassName('enemy')
	for (let i = 0; i < enemys.length; i++) {
		if ((myPlane.offsetLeft < enemys[i].offsetLeft + enemys[i].offsetWidth) && (myPlane.offsetLeft + 66 > enemys[i].offsetLeft) &&
			(myPlane.offsetTop < enemys[i].offsetTop + enemys[i].offsetHeight) && (myPlane.offsetTop + 80 > enemys[i].offsetTop)
		) {
			myPlane.src = './image/本方飞机爆炸.gif'
			if (myPlane.getAttribute('myph') <= 0 || pphh.value <= 0) {
				clearInterval(timer)
				stopGame.style.display = 'block'
				last.innerHTML = score.innerText
				myPlane.className = 'finish'
				break
			} else {
				setTimeout(function() {
					myPlane.setAttribute('myph', parseInt(myPlane.getAttribute('myph')) - 2)
					pphh.value = parseInt(pphh.value) - 2
				}, 600)
			}
			setTimeout(function() {
				myPlane.src = './image/我的飞机.gif'
			}, 800)
		}
	}
}
start.addEventListener('click', function() {
	cover.style.display = 'none'
	let mainBgdPstY = 0
	let num = 0
	timer = setInterval(function() {
		//删掉已经完成使命的标签
		const finishs = main.getElementsByClassName('finish')
		for (let i = 0; i < finishs.length; i++) {
			finishs[i].remove()
		}
		//背景的移动
		if (mainBgdPstY > 568) {
			mainBgdPstY = 0
		}
		mainBgdPstY++
		main.style.backgroundPositionY = mainBgdPstY + 'px'
		//生成子弹
		const bulletImg = document.createElement('img')
		bulletImg.src = './image/bullet1.png'
		bulletImg.className = 'bullet'
		bulletImg.style.left = myPlane.offsetLeft + 30 + 'px'
		bulletImg.style.top = myPlane.offsetTop + 'px'
		main.appendChild(bulletImg)
		if(parseInt(myPlane.getAttribute('three'))) {
			myPlane.innerText 
			const bulletImg1 = document.createElement('img')
			bulletImg1.src = './image/bullet1.png'
			bulletImg1.className = 'bullet'
			bulletImg1.style.left = myPlane.offsetLeft + 10 + 'px'
			bulletImg1.style.top = myPlane.offsetTop + 30 + 'px'
			main.appendChild(bulletImg1)
			const bulletImg2 = document.createElement('img')
			bulletImg2.src = './image/bullet1.png'
			bulletImg2.className = 'bullet'
			bulletImg2.style.left = myPlane.offsetLeft + 50 + 'px'
			bulletImg2.style.top = myPlane.offsetTop + 30 + 'px'
			main.appendChild(bulletImg2)
			myPlane.setAttribute('three', parseInt(myPlane.getAttribute('three'))-1)
		}
		//生成小敌机
		if (num % 2 === 0) {
			const small = document.createElement('img')
			small.src = './image/enemy1_fly_1.png'
			small.className = 'small enemy'
			small.style.left = Math.random() * 286 + 'px'
			small.style.top = '-24px'
			main.appendChild(small)
		}
		//生成中飞机
		if (num % 10 === 0) {
			const middle = document.createElement('img')
			middle.src = './image/enemy3_fly_1.png'
			middle.className = 'middle enemy'
			middle.style.left = Math.random() * 274 + 'px'
			middle.style.top = '-60px'
			middle.setAttribute('ph', 5)
			main.appendChild(middle)
		}
		//大型机
		if (num % 20 === 0) {
			const big = document.createElement('img')
			big.src = './image/enemy2_fly_1.png'
			big.className = 'big enemy'
			big.style.left = Math.random() * 210 + 'px'
			big.style.top = '-164px'
			big.setAttribute('ph', 10)
			main.appendChild(big)
		}
		//移动子弹
		const bullets = main.getElementsByClassName('bullet')
		for (let i = 0; i < bullets.length; i++) {
			let bulletOver = false
			bullets[i].style.top = bullets[i].offsetTop - 14 + 'px'
			//打下飞机
			const smalls = main.getElementsByClassName('small')
			for (let j = 0; j < smalls.length; j++) {
				if ((bullets[i].offsetLeft + 6 > smalls[j].offsetLeft) && (bullets[i].offsetLeft < smalls[j].offsetLeft + 34) &&
					(bullets[i].offsetTop < smalls[j].offsetTop + 24) && (bullets[i].offsetTop + 14 > smalls[j].offsetTop)) {
					smalls[j].src = './image/xiaobaozha.gif'
					bullets[i].style.display = 'none'
					bullets[i].className = 'bullet finish'
					smalls[j].className = 'small finish'
					bulletOver = true
					score.innerText = parseInt(score.innerText) + 1
					break
				}
			}
			if (bulletOver) {
				continue
			}
			//打中飞机
			const middles = main.getElementsByClassName('middle')
			for (let k = 0; k < middles.length; k++) {
				if ((bullets[i].offsetLeft < middles[k].offsetLeft + 46) && (bullets[i].offsetLeft + 6 > middles[k].offsetLeft) &&
					(bullets[i].offsetTop < middles[k].offsetTop + 60) && (bullets[i].offsetTop + 14 > middles[k].offsetTop)) {
					middles[k].setAttribute('ph', middles[k].getAttribute('ph') - 1)
					if (middles[k].getAttribute('ph') <= 0) {
						middles[k].src = './image/zhongbaozha.gif'
						middles[k].className = 'middle finish'
						bullets[i].style.display = 'none'
						score.innerText = parseInt(score.innerText) + 2

					} else {
						middles[k].src = './image/zhongaida.png'
					}
					bullets[i].className = 'bullet finish'
					bulletOver = true
					break
				}
			}
			if (bulletOver) {
				continue
			}
			//打大飞机
			const bigs = main.getElementsByClassName('big')
			for (let k = 0; k < bigs.length; k++) {
				if ((bullets[i].offsetLeft < bigs[k].offsetLeft + bigs[k].offsetWidth) && (bullets[i].offsetLeft + 6 > bigs[k].offsetLeft) &&
					(bullets[i].offsetTop < bigs[k].offsetTop + bigs[k].offsetHeight) && (bullets[i].offsetTop + 14 > bigs[k].offsetTop)) {
					bigs[k].setAttribute('ph', parseInt(bigs[k].getAttribute('ph')) - 1)
					if (bigs[k].getAttribute('ph') <= 0) {
						bigs[k].src = './image/dabaozha.gif'
						bigs[k].className = 'big finish'
						score.innerText = parseInt(score.innerText) + 3
						let a = Math.floor(Math.random() * 100 % 6)
						if (a) {
							let buff = document.createElement('div')
							buff.className = 'buff'
							if (a === 1) {
								buff.style.backgroundColor = 'red'
							} else if (a === 2) {
								buff.style.backgroundColor = 'blue'
							} else if (a === 3) {
								buff.style.backgroundColor = 'green'
							}
							buff.style.left = bigs[k].offsetLeft + 50 + 'px'
							buff.style.top = bigs[k].offsetTop + 77 + 'px'
							buff.className = 'buff'
							main.appendChild(buff)
						}
					} else {
						bigs[k].src = './image/daaida.png'
						bullets[i].style.display = 'none'
					}
					bullets[i].className = 'finish bullet '
					bulletOver = true
					break
				}
			}
			if (bulletOver) {
				continue
			}
			// 吃 bug
			const buffs = document.getElementsByClassName('buff')
			for (let i = 0; i < buffs.length; i++) {
				if ((myPlane.offsetLeft < buffs[i].offsetLeft + 10) && (myPlane.offsetLeft + 66 > buffs[i].offsetLeft) &&
					(myPlane.offsetTop < buffs[i].offsetTop + 10) && (myPlane.offsetTop + 80 > buffs[i].offsetHeight)
				) {
					let bulltNum = 0
					if(buffs[i].style.backgroundColor == 'red') {
						const smalls = document.getElementsByClassName('small');
						for(let i=0; i<smalls.length; i++) {
							smalls[i].src = './image/xiaobaozha.gif'
							score.innerText = parseInt(score.innerText)+1
							smalls[i].className = 'finish small'
						}
						const middles = document.getElementsByClassName('middle')
						for(let i=0; i<middles.length; i++) {
							middles[i].src = './image/zhongbaozha.gif'
							score.innerText = parseInt(score.innerText)+2
							middles[i].className = 'finish middle'
						}
						const bigs= document.getElementsByClassName('big')
						for(let j=0; j<bigs.length; j++) {
							bigs[j].src = './image/dabaozha.gif'
							score.innerText = parseInt(score.innerText)+3
							bigs[j].className = 'finish big'
						}
					}else if(buffs[i].style.backgroundColor == 'blue') {
						myPlane.setAttribute('three', '100')
						
					}else if(buffs[i].style.backgroundColor == 'green') {
						myPlane.setAttribute('wudi', 100)
						pphh.value = parseInt(pphh.value) + 5
						myPlane.setAttribute('myph', parseInt(myPlane.getAttribute('pphh'))+5)
					}
					buffs[i].className = 'buff finish'
					break
				}
			}
			
			if (bullets[i].offsetTop <= -14) {
				bullets[i].className = 'bullet finish'
			}

		}
		//移动小敌机
		const smalls = main.getElementsByClassName('small')
		for (let i = 0; i < smalls.length; i++) {
			smalls[i].style.top = smalls[i].offsetTop + 20 + 'px'
			if (smalls[i].offsetTop >= 568) {
				smalls[i].className = 'small finish'
			}
		}
		//移动中级机
		const middles = main.getElementsByClassName('middle')
		for (let i = 0; i < middles.length; i++) {
			middles[i].style.top = middles[i].offsetTop + 10 + 'px'
			if (middles[i].offsetTop >= 568) {
				middles[i].className = 'middle finish'
			}
		}
		//移动大型机机
		const bigs = main.getElementsByClassName('big')
		for (let i = 0; i < bigs.length; i++) {
			bigs[i].style.top = bigs[i].offsetTop + 20 + 'px'
			if (bigs[i].offsetTop >= 568) {
				bigs[i].className = 'big finish'
			}
		}
		//限制敌机数量
		num++
		if (num > 100) {
			num = 0
		}
		//无敌
		let touming = false
		if(myPlane.getAttribute('wudi') > 0) {
			myPlane.style.opacity = .5
			myPlane.setAttribute('wudi',parseInt(myPlane.getAttribute('wudi'))-1)
			if(myPlane.getAttribute('wudi') >= 30) {
				touming = true
			}else {
				if(myPlane.getAttribute('wudi') %2 == 0) {
					touming = !touming
				}
			}
		}else {
			touming = false
			gameover()
		}
		if(touming) {
			myPlane.style.opacity = .5
		}else {
			myPlane.style.opacity = 1
		}
	}, 100)
})
main.addEventListener('mousemove', function(e) {
	myPlane.style.left = e.offsetX - 33 + 'px'
	myPlane.style.top = e.offsetY - 40 + 'px'
	if (myPlane.offsetLeft < 0) {
		myPlane.style.left = '0px'
	} else if (myPlane.offsetLeft > 254) {
		myPlane.style.left = '254px'
	}
	if (myPlane.offsetTop < 0) {
		myPlane.style.top = '0px'
	} else if (myPlane.offsetTop > 488) {
		myPlane.style.top = '488px'
	}
	window.onload = function() {
		if(myPlane.getAttribute('wudi') <= 0) {
			gameover()
		}
	}
})
